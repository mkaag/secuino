/*
 * General Configuration
 */
#define SERIAL_BAUD 9600
#define VERSION_STRING "0.2"
#define ENABLE_ALERT
#define ENABLE_DEBUG

/*
 * GSM Configuration
 */
#define PINNUMBER ""
char phoneNb[20] = "";
char textMessage[200] = "";
GSM gsmAccess;
GSM_SMS sms;

/*
 * Network Configuration
 */
static uint8_t mac[] = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };
static uint8_t ip[] = { 192, 168, 1, 100 };

/*
 * WebServer Configuration
 */
#define PREFIX ""
#define TCP_HTTP 80
WebServer webserver(PREFIX, TCP_HTTP);

