/*
  ** Secunio **
  This sketch, for the Arduino Board Ethernet, listens for incoming
  http query and trigger a notification using GSM network.
 
  Creation date:  November 25, 2013 
  Author:         Maurice Kaag (https://github.com/mkaag/)
  Language:       Arduino
  License:        GPL v3 or later
 
  Circuit:
  * Board Ethernet R3 (http://arduino.cc/en/Main/ArduinoBoardEthernet)
  * GSM Shield (http://arduino.cc/en/Main/ArduinoGSMShield)
  
  Dependencies:
  * https://github.com/sirleech/Webduino

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "SPI.h"
//#include "avr/pgmspace.h"
#include "Ethernet.h"
#include "WebServer.h"
#include "GSM.h"
#include "config.h"

P(Page_start) = "<html><head><title>Secunio " VERSION_STRING "</title></head><body>\n";
P(Page_end) = "</body></html>";
P(Get_head) = "GET ";
P(Post_head) = "POST ";
P(Unknown_head) = "<h1>UNKNOWN request for ";
P(Default_head) = "<h1>Welcome to Secunio</h1><br>\n";
P(Parsed_head) = "API<br>\n";
P(Good_tail_begin) = "URL tail = '";
P(Bad_tail_begin) = "INCOMPLETE URL tail = '";
P(Tail_end) = "'<br>\n";
P(Parsed_tail_begin) = "URL parameters:<br>\n";
P(Parsed_item_separator) = " = '";
P(Params_end) = "End of parameters<br>\n";
P(Post_params_begin) = "Parameters sent by POST:<br>\n";
P(Line_break) = "<br>\n";

/*
 * A Better Serial.print() For Arduino
 * Posted on August 8, 2011 by David Pankhurst
 * http://www.utopiamechanicus.com/article/low-memory-serial-print/
 */
void StreamPrint_progmem(Print &out,PGM_P format,...)
{
  // program memory version of printf - copy of format string and result share a buffer
  // so as to avoid too much memory use
  char formatString[128], *ptr;
  strncpy_P( formatString, format, sizeof(formatString) ); // copy in from program mem
  // null terminate - leave last char since we might need it in worst case for result's \0
  formatString[ sizeof(formatString)-2 ] = '\0'; 
  ptr = &formatString[ strlen(formatString)+1 ]; // our result buffer...
  va_list args;
  va_start (args, format);
  vsnprintf(ptr, sizeof(formatString)-1-strlen(formatString), formatString, args );
  va_end (args);
  formatString[ sizeof(formatString)-1 ] = '\0';
  #ifdef ENABLE_DEBUG
    out.print(ptr);
  #endif
}
#define Serialprint(format, ...) StreamPrint_progmem(Serial, PSTR(format), ##__VA_ARGS__)
#define Streamprint(stream, format, ...) StreamPrint_progmem(stream, PSTR(format), ##__VA_ARGS__)

/* Commands are functions that get called by the webserver framework
 * they can read any posted data from client, and they output to the
 * server to send data back to the web browser. 
 */
void indexCmd(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete)
{
  Serialprint("Incoming request to the Index...\n");
  server.httpSuccess();

  // If we're handling a GET or POST, we can output our data here.
  // For a HEAD request, we just stop after outputting headers.
  if (type == WebServer::HEAD)
    return;

  server.printP(Page_start);
  server.printP(Default_head);
  switch (type)
  {
    case WebServer::GET:
      server.printP(Get_head);
      break;
    case WebServer::POST:
      server.printP(Post_head);
      break;
    default:
      server.printP(Unknown_head);
  }

  server.printP(tail_complete ? Good_tail_begin : Bad_tail_begin);
  server.print(url_tail);
  server.printP(Tail_end);
  server.printP(Page_end);
}

#define NAMELEN 32
#define VALUELEN 32
void parsedCmd(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete)
{
  URLPARAM_RESULT rc;
  char name[NAMELEN];
  char value[VALUELEN];

  Serialprint("Incoming request to the API...\n");
  server.httpSuccess();

  // If we're handling a GET or POST, we can output our data here.
  // For a HEAD request, we just stop after outputting headers.
  if (type == WebServer::HEAD)
    return;

  server.printP(Page_start);
  server.printP(Parsed_head);
  switch (type)
  {
    case WebServer::GET:
      server.printP(Get_head);
      break;
    case WebServer::POST:
      server.printP(Post_head);
      break;
    default:
      server.printP(Unknown_head);
  }

  server.printP(tail_complete ? Good_tail_begin : Bad_tail_begin);
  server.print(url_tail);
  server.printP(Tail_end);

  if (strlen(url_tail))
  {
    server.printP(Parsed_tail_begin);
    while (strlen(url_tail))
    {
      rc = server.nextURLparam(&url_tail, name, NAMELEN, value, VALUELEN);
      if (rc == URLPARAM_EOS)
        server.printP(Params_end);
      else
      {
        server.print(name);
        server.printP(Parsed_item_separator);
        server.print(value);
        server.printP(Tail_end);
      }
    }
  }
  if (type == WebServer::POST)
  {
    server.printP(Post_params_begin);
    while (server.readPOSTparam(name, NAMELEN, value, VALUELEN))
    {
      server.print(name);
      server.printP(Parsed_item_separator);
      server.print(value);
      server.printP(Tail_end);
    }
  }
  server.printP(Page_end);
  
  // If alert is enabled and type of alert is motion, then trigger notification
  // TODO: parse correctly the POST
  #ifdef ENABLE_ALERT
    Serialprint("Sending SMS notification...");
    sms.beginSMS(phoneNb);
    sms.print(textMessage);
    sms.endSMS();
    Serialprint("done\n");
  #endif
}

void my_failCmd(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete)
{
  Serialprint("Incoming request failed...\n");
  server.httpFail();

  // If we're handling a GET or POST, we can output our data here.
  // For a HEAD request, we just stop after outputting headers.
  if (type == WebServer::HEAD)
    return;

  server.printP(Page_start);
  server.printP(Default_head);
  switch (type)
  {
    case WebServer::GET:
      server.printP(Get_head);
      break;
    case WebServer::POST:
      server.printP(Post_head);
      break;
    default:
      server.printP(Unknown_head);
  }

  server.printP(tail_complete ? Good_tail_begin : Bad_tail_begin);
  server.print(url_tail);
  server.printP(Tail_end);
  server.printP(Page_end);
}

void setup()
{
  // initialize serial communications and wait for port to open:
  Serial.begin(SERIAL_BAUD);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  Serialprint("\nBooting system...\n");
  Serialprint("Initializing network...\n");
  Ethernet.begin(mac, ip);
  //Streamprint(Serial, "using IP %d\n", Ethernet.localIP());

  Serialprint("Starting HTTP Server...");
  webserver.setDefaultCommand(&indexCmd);
  webserver.setFailureCommand(&my_failCmd);
  webserver.addCommand("api", &parsedCmd);
  webserver.begin();
  Streamprint(Serial, "running on TCP %d\n", TCP_HTTP);

  Serialprint("Initializing GSM...");
  boolean notConnected = true;

  // Start GSM shield
  // If your SIM has PIN, pass it as a parameter of begin() in quotes
  while(notConnected)
  {
    if(gsmAccess.begin(PINNUMBER)==GSM_READY) 
    {
      Serialprint("ok\n");
      notConnected = false;
    } 
    else {
      Serialprint("Not connected\n");
      delay(1000);
    }
  }
}

void loop()
{
  char buff[64];
  int len = 64;

  // Process incoming connections one at a time forever
  webserver.processConnection(buff, &len);
}

